#########################################
Hb Bank Statement Machine Learning Module
#########################################

...


*******
Install
*******

Dependencies for ArchLinux

.. code-block::

    sudo pacman -S cairo pkgconf gobject-introspection


Dependencies for debian


.. code-block::

    sudo apt-get install libcairo2-dev libgirepository1.0-dev


Install the package

.. code-block::

    # installs python deps
    pip install hb_tryton_hr_expense
    # install the module 
    trytond-admin -u hr_expense --activate-dependencies


Install the db by hb-tryton-devtools

.. code-block::

    pip install git+https://gitlab.com/hashbangfr/tryton-modules/hb_tryton_devtools.git#egg=hb_tryton_devtools
    export TRYTON_DATABASE_URI=postgresql:///
    export TRYTON_DATABASE_NAME=test
    hb-tryton-admin create-db --modules hr_expense


************
Test package
************

The package need pytest and hb-tryton-devtools

.. code-block::

    pip install pytest pytest-cov
    pip install git+ssh://git@gitlab.com/hashbangfr/tryton-modules/hb_tryton_devtools.git#egg=hb_tryton_devtools


Run the test with pytest with environ variable

.. code-block::

    export TRYTON_DATABASE_URI=postgresql:///
    export TRYTON_DATABASE_NAME=test
    pytest hr_expense/tests

*****
Usage
*****

This application allows you to manage your employees's daily expenses. It 
gives you access to your employees’s fee notes and give you the right to 
complete and validate or refuse the notes. After validation it creates an 
invoice for the employee. Employee can encode their own expenses and the 
validation flow puts it automatically in the accounting after validation by 
managers.


*********
CHANGELOG
*********

0.0.1 (unreleased)
------------------

* 
