from trytond.pool import Pool
from . import configuration
from . import expense
from . import product
from . import account

__all__ = ['register']


def register():
    Pool.register(
        product.Template,
        configuration.Configuration,
        configuration.ConfigurationCompany,
        expense.HrExpense,
        expense.HrExpenseLine,
        account.Move,
        account.Line,
        module='hr_expense', type_='model')
    Pool.register(
        module='hr_expense', type_='wizard')
    Pool.register(
        expense.ZipReport,
        module='hr_expense', type_='report')
