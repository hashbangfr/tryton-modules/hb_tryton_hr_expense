import pytest  # noqa
from decimal import Decimal as D
from datetime import date
from trytond.exceptions import UserWarning, UserError
from hb.tryton.devtools.helper.accounts import fiscal_year
from hb.tryton.devtools.helper.accounts.fr import import_accounts_fr
from hb.tryton.devtools.tests.testing import get_company

year = date.today().year


class TestExpense:

    def import_accounts(self, company):
        import_accounts_fr(company.id)
        fiscal_year(company=company, state_date=date(year, 1, 1))

    def configure_mission(self, pool):
        Account = pool.get('account.account')
        Config = pool.get('hr.expense.configuration')

        account_mission, = Account.search([('code', '=', '6256')])
        account_expense, = Account.search([('code', '=', '425')])

        # config
        config = Config(1)
        config.hr_expense_account = account_expense
        config.save()
        return account_mission

    def create_employee(self, pool):
        User = pool.get('res.user')
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')

        party = Party(name='Employee')
        party.save()
        employee = Employee(party=party)
        employee.save()
        user, = User.search([], limit=1)
        user.employees = [employee]
        user.save()
        user.employee = employee
        user.save()
        return employee

    def create_supplier(self, pool):
        Party = pool.get('party.party')
        supplier = Party(name='Supplier')
        supplier.save()
        return supplier

    def create_product(self, pool):
        Categ = pool.get('product.category')
        Uom = pool.get('product.uom')
        Template = pool.get('product.template')
        Product = pool.get('product.product')

        account_mission = self.configure_mission(pool)
        categ = Categ(
            name='Mission', account_expense=account_mission,
            accounting=True)
        categ.save()
        uom, = Uom.search([], limit=1)
        template = Template(
            name='Mission',
            default_uom=uom,
            account_category=categ,
            is_hr_expense=True)
        template.save()
        product = Product(
            code='Mission',
            template=template)
        product.save()
        return product

    def test_expense_on_change(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            expense.on_change_lines()
            line.on_change_with_expense_state()

    def test_expense_report_without_attachment(self, rollbacked_transaction,
                                               pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            with pytest.raises(UserWarning):
                Expense.report([expense])

    def test_expense_report_redraft(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            assert expense.state == 'draft'
            Expense.report([expense])
            assert expense.state == 'reported'
            Expense.redraft([expense])
            assert expense.state == 'draft'

    def test_expense_report_refused_redraft(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            assert expense.state == 'draft'
            Expense.report([expense])
            assert expense.state == 'reported'
            Expense.refuse([expense])
            assert expense.state == 'refused'
            assert expense.refused_by == employee
            Expense.redraft([expense])
            assert expense.state == 'draft'
            assert expense.refused_by is None

    def test_expense_report_approve_do(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()
            assert expense.untaxed_amount_cache is None
            assert expense.tax_amount_cache is None
            assert expense.total_amount_cache is None
            assert expense.untaxed_amount == D('0')
            assert expense.tax_amount == D('0')
            assert expense.total_amount == D('0')

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            assert line.untaxed_amount == D('100')
            assert line.tax_amount == D('20')
            assert line.total_amount == D('120')
            assert expense.untaxed_amount_cache is None
            assert expense.tax_amount_cache is None
            assert expense.total_amount_cache is None
            assert expense.untaxed_amount == D('100')
            assert expense.tax_amount == D('20')
            assert expense.total_amount == D('120')

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            assert expense.state == 'draft'
            Expense.report([expense])
            assert expense.untaxed_amount_cache is None
            assert expense.tax_amount_cache is None
            assert expense.total_amount_cache is None
            assert expense.state == 'reported'
            assert expense.approved_by is None
            Expense.approve([expense])
            assert expense.untaxed_amount_cache == D('100')
            assert expense.tax_amount_cache == D('20')
            assert expense.total_amount_cache == D('120')
            assert expense.untaxed_amount == D('100')
            assert expense.tax_amount == D('20')
            assert expense.total_amount == D('120')
            assert expense.state == 'approved'
            assert expense.approved_by == employee
            Expense.do([expense])
            assert expense.state == 'done'

    def test_expense_delete_draft(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()
            Expense.delete([expense])

    def test_expense_delete_reported(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            Expense.report([expense])
            with pytest.raises(UserError):
                Expense.delete([expense])

    def test_expense_delete_refused(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            Expense.report([expense])
            Expense.refuse([expense])
            with pytest.raises(UserError):
                Expense.delete([expense])

    def test_expense_delete_approved(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            Expense.report([expense])
            Expense.approve([expense])
            with pytest.raises(UserError):
                Expense.delete([expense])

    def test_expense_delete_done(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            Expense.report([expense])
            Expense.approve([expense])
            Expense.do([expense])
            with pytest.raises(UserError):
                Expense.delete([expense])

    def test_expense_zip_report(self, rollbacked_transaction, pool):
        Expense = pool.get('hr.expense')
        ExpenseLine = pool.get('hr.expense.line')

        Attachment = pool.get('ir.attachment')
        ZipReport = pool.get('hr.expense.zipreport', type='report')

        company = get_company()
        with rollbacked_transaction.set_context(company=company.id):
            self.import_accounts(company)
            # accounts
            employee = self.create_employee(pool)
            supplier = self.create_supplier(pool)
            product = self.create_product(pool)

            # create expense
            expense = Expense(employee=employee, expense_date=date(year, 6, 1))
            expense.save()
            assert expense.untaxed_amount_cache is None
            assert expense.tax_amount_cache is None
            assert expense.total_amount_cache is None
            assert expense.untaxed_amount == D('0')
            assert expense.tax_amount == D('0')
            assert expense.total_amount == D('0')

            # create expense line
            line = ExpenseLine(
                expense=expense,
                party=supplier,
                product=product,
                reference='test',
                untaxed_amount=100,
                tax_amount=20,
                total_amount=120,
            )
            line.save()

            assert line.untaxed_amount == D('100')
            assert line.tax_amount == D('20')
            assert line.total_amount == D('120')
            assert expense.untaxed_amount_cache is None
            assert expense.tax_amount_cache is None
            assert expense.total_amount_cache is None
            assert expense.untaxed_amount == D('100')
            assert expense.tax_amount == D('20')
            assert expense.total_amount == D('120')

            Attachment(
                name='Attachment',
                resource=expense,
                type='link',
                link='http://link',
            ).save()

            res = ZipReport.execute([expense.id], {})
            assert res[0] == 'zip'
            assert res[1]
            assert res[2] is False
            assert res[3]
