import pytest  # noqa
from hb.tryton.devtools.tests.testing import MixinTestModule


class TestModule(MixinTestModule):
    module = 'hr_expense'
