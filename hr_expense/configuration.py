from trytond.model import (
    ModelView, ModelSQL, ModelSingleton, ValueMixin, fields)
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.modules.company.model import CompanyMultiValueMixin

sale_invoice_method = fields.Selection(
    'get_sale_invoice_methods', "Sale Invoice Method")
sale_shipment_method = fields.Selection(
    'get_sale_shipment_methods', "Sale Shipment Method")


def default_func(field_name):

    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()

    return default


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'hr expense  Configuration'
    __name__ = 'hr.expense.configuration'
    journal = fields.MultiValue(
        fields.Many2One(
            'account.journal', 'Journal', required=True,
            domain=[('type', '=', 'expense')]))
    hr_expense_account = fields.MultiValue(
        fields.Many2One(
            'account.account', 'Hr expense account', required=True,
            domain=[
                ('closed', '!=', True),
                ('type.payable', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
            ]))
    tax_account = fields.MultiValue(
        fields.Many2One(
            'account.account', 'Tax account', required=True,
            domain=[
                ('closed', '!=', True),
                ('type.payable', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
            ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'journal', 'hr_expense_account', 'tax_account'}:
            return pool.get('hr.expense.configuration.company')
        return super(Configuration, cls).multivalue_model(field)

    default_journal = default_func('journal')
    default_hr_expense_account = default_func('hr_expense_account')
    default_tax_account = default_func('tax_account')


class ConfigurationCompany(ModelSQL, ValueMixin):
    'hr expense  Configuration by company'
    __name__ = 'hr.expense.configuration.company'
    journal = fields.Many2One(
        'account.journal', 'Journal', required=True,
        domain=[('type', '=', 'expense')])
    hr_expense_account = fields.Many2One(
        'account.account', 'Hr expense account', required=True,
        domain=[
            ('closed', '!=', True),
            ('type.payable', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])
    tax_account = fields.Many2One(
        'account.account', 'Tax account', required=True,
        domain=[
            ('closed', '!=', True),
            ('type.payable', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])

    @classmethod
    def default_journal(cls):
        pool = Pool()
        AccountJournal = pool.get('account.journal')
        journal, = AccountJournal.search([('type', '=', 'expense')], limit=1)
        return journal.id

    @classmethod
    def default_hr_expense_account(cls):
        pool = Pool()
        AccountAccount = pool.get('account.account')
        accounts = AccountAccount.search([('code', 'like', '4251')])
        return accounts[0].id if len(accounts) else None

    @classmethod
    def default_tax_account(cls):
        pool = Pool()
        AccountAccount = pool.get('account.account')
        accounts = AccountAccount.search([('code', 'like', '44566')])
        return accounts[0].id if len(accounts) else None
