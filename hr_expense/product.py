from trytond.model import fields
from trytond.pool import PoolMeta


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    is_hr_expense = fields.Boolean("Is for expense")
