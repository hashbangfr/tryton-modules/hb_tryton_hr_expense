import tempfile
import csv
from decimal import Decimal
from datetime import date
from os import makedirs, path
from shutil import make_archive
from trytond.exceptions import UserWarning, UserError
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.model import (
    ModelSQL, ModelView, Workflow, fields, sequence_ordered
)
from trytond.pyson import Eval, Bool
from trytond.modules.company.model import (
    employee_field, set_employee, reset_employee)
from trytond.modules.currency.fields import Monetary
from trytond.i18n import gettext
from trytond.report import Report


class HrExpense(Workflow, ModelSQL, ModelView):
    "Expense sheet for the employee"
    __name__ = 'hr.expense'

    company = fields.Many2One(
        'company.company', "Company", required=True,
        states={
            'readonly': (Eval('state') != 'draft') | Eval('employee', True),
        },
        depends=['state', 'employee'])
    employee = fields.Many2One(
        'company.employee', "Employee",
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': (Eval('state') != 'draft'),
        },
        depends=['company', 'state'])
    expense_date = fields.Date("Date", required=True)
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('reported', 'Submitted'),
        ('approved', 'Approved'),
        ('done', 'Paid'),
        ('refused', 'Refused')], "State", readonly=True, required=True,
        help="The current state of the sheet.")
    approved_by = employee_field(
        "Approved By",
        states=['draft', 'reported', 'approved', 'refused', 'done'],
        company='company')
    refused_by = employee_field(
        "Refused By",
        states=['draft', 'reported', 'approved', 'refused', 'done'],
        company='company')
    lines = fields.One2Many(
        'hr.expense.line', 'expense', "Lines",
        states={
            'readonly': (
                (Eval('state') != 'draft')
                | ~Eval('employee')
                | ~Eval('expense_date')
            ),
        },
        depends=['state', 'employee', 'expense_date'])
    currency = fields.Many2One(
        'currency.currency', "Currency", required=True,
        states={
            'readonly': (
                (Eval('state') != 'draft')
                | (Eval('lines', [0]) & Eval('currency', 0))
            ),
        },
        depends=['state'])
    untaxed_amount = fields.Function(
        Monetary("Untaxed", digits='currency', currency='currency'),
        'get_amount')
    untaxed_amount_cache = fields.Numeric(
        "Untaxed Cache", digits='currency', readonly=True)
    tax_amount = fields.Function(
        Monetary("Tax", digits='currency', currency='currency'),
        'get_amount')
    tax_amount_cache = fields.Numeric(
        "Tax Cache", digits='currency', readonly=True)
    total_amount = fields.Function(
        Monetary("Total", digits='currency', currency='currency'),
        'get_amount')
    total_amount_cache = fields.Numeric(
        "Total Cache", digits='currency', readonly=True)

    @classmethod
    def __setup__(cls):
        super(HrExpense, cls).__setup__()
        cls._order = [
            ('expense_date', 'DESC'),
            ('id', 'DESC'),
        ]
        cls._transitions |= set((
            ('draft', 'reported'),
            ('reported', 'draft'),
            ('reported', 'approved'),
            ('reported', 'refused'),
            ('refused', 'draft'),
            ('approved', 'done'),
        ))
        cls._buttons.update({
            'redraft': {
                'invisible': ~Eval('state').in_(['reported', 'refused']),
                'icon': 'tryton-cancel',
                'depends': ['state'],
            },
            'report': {
                'invisible': Eval('state') != 'draft',
                'readonly': ~Eval('lines', []),
                'icon': 'tryton-forward',
                'depends': ['state'],
            },
            'approve': {
                'invisible': Eval('state') != 'reported',
                'icon': 'tryton-launch',
                'depends': ['state'],
            },
            'refuse': {
                'invisible': Eval('state') != 'reported',
                'icon': 'tryton-cancel',
                'depends': ['state'],
            },
        })
        cls._states_cached = ['reported', 'approved', 'done', 'refused']

    @classmethod
    def get_amount(cls, expenses, names):
        untaxed_amount = {}
        tax_amount = {}
        total_amount = {}

        # Sort cached first and re-instanciate to optimize cache management
        expenses = sorted(
            expenses, key=lambda s: s.state in cls._states_cached, reverse=True)
        expenses = cls.browse(expenses)
        for expense in expenses:
            if (
                expense.state in cls._states_cached
                and expense.untaxed_amount_cache is not None
                and expense.tax_amount_cache is not None
                and expense.total_amount_cache is not None
            ):
                untaxed_amount[expense.id] = expense.untaxed_amount_cache
                tax_amount[expense.id] = expense.tax_amount_cache
                total_amount[expense.id] = expense.total_amount_cache
            else:
                untaxed_amount[expense.id] = sum(
                    (line.untaxed_amount for line in expense.lines), Decimal(0))
                tax_amount[expense.id] = sum(
                    (line.tax_amount for line in expense.lines), Decimal(0))
                total_amount[expense.id] = sum(
                    (line.total_amount for line in expense.lines), Decimal(0))

        result = {
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
        }
        return {
            k: v
            for k, v in result.items()
            if k in names
        }

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_employee(cls):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        return user.employee.id if user.employee else None

    @classmethod
    def default_expense_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_currency(cls):
        pool = Pool()
        Company = pool.get('company.company')
        company = cls.default_company()
        if company:
            return Company(company).currency.id

    @fields.depends('lines', 'currency')
    def on_change_lines(self):
        self.untaxed_amount = Decimal('0.0')
        self.tax_amount = Decimal('0.0')
        self.total_amount = Decimal('0.0')

        if self.lines:
            for line in self.lines:
                self.untaxed_amount += getattr(
                    line, 'untaxed_amount', None) or 0
                self.tax_amount += getattr(line, 'tax_amount', None) or 0
                self.total_amount += getattr(line, 'total_amount', None) or 0

        if self.currency:
            self.untaxed_amount = self.currency.round(self.untaxed_amount)
            self.tax_amount = self.currency.round(self.tax_amount)
            self.total_amount = self.currency.round(self.total_amount)

    @classmethod
    def store_cache(cls, expenses):
        for expense in expenses:
            expense.untaxed_amount_cache = expense.untaxed_amount
            expense.tax_amount_cache = expense.tax_amount
            expense.total_amount_cache = expense.total_amount

        cls.save(expenses)

    def get_purchase_journal(self):
        pool = Pool()
        config = pool.get('hr.expense.configuration')(1)
        return config.journal

    def get_supplier_account(self):
        pool = Pool()
        Account = pool.get('account.account')
        account, = Account.search([('code', '=', '4011')])
        return account

    def get_hr_expense_account(self):
        pool = Pool()
        config = pool.get('hr.expense.configuration')(1)
        return config.hr_expense_account

    def create_account_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')
        label = (
            f"{self.expense_date.month}/{self.expense_date.year} "
            f"{self.employee.party.name}")
        period = Period.find(self.company.id, date=self.expense_date)
        movelines = []
        hr_expense_account = self.get_hr_expense_account()
        supplier_account = self.get_supplier_account()
        for line in self.lines:
            hr_expense_line_value = dict(
                description=line.reference,
                origin=line,
                account=hr_expense_account,
                credit=line.total_amount,
            )
            if hr_expense_account.party_required:
                hr_expense_line_value['party'] = line.party

            movelines.extend([
                Line(description=line.reference,
                     origin=line,
                     account=supplier_account,
                     debit=line.total_amount,
                     party=line.party),
                Line(**hr_expense_line_value),
            ])

        move = Move(
            journal=self.get_purchase_journal(),
            date=self.expense_date,
            description=label,
            lines=movelines,
            period=period,
            origin=self,
        )
        move.save()
        return move

    @classmethod
    def delete(cls, records):
        invalidated_record = False
        for record in records:
            if record.state != 'draft':
                invalidated_record = True

        if invalidated_record:
            raise UserError(gettext('hr_expense.not_draft_expenses'))

        return super().delete(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    @reset_employee('approved_by', 'refused_by')
    def redraft(cls, expenses):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('reported')
    def report(cls, expenses):
        pool = Pool()

        expenses_without_attachment = []
        for expense in expenses:
            resources = expense.resources()
            if not resources['attachment_count']:
                expenses_without_attachment.append(expense)

        if expenses_without_attachment:
            Warning = pool.get('res.user.warning')
            warning_expenses = Warning.format(
                'expense_without_account',
                expenses_without_attachment)

            if Warning.check(warning_expenses):
                raise UserWarning(
                    warning_expenses, gettext(
                        'hr_expense.expenses_without_attachment'))

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    @set_employee('approved_by')
    def approve(cls, expenses):
        cls.store_cache(expenses)
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        for expense in expenses:
            moves = [expense.create_account_move()]
            for line in expense.lines:
                moves.append(line.create_account_move())

            Move.post(moves)
            lines = {}
            supplier_account = expense.get_supplier_account()
            for move in moves:
                for line in move.lines:
                    if line.account != supplier_account:
                        continue

                    if line.party.id not in lines:
                        lines[line.party.id] = []

                    lines[line.party.id].append(line)

            lines = [x for x in lines.values()]
            Line.reconcile(*lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('refused')
    @set_employee('refused_by')
    def refuse(cls, expenses):
        pass

    @classmethod
    @Workflow.transition('done')
    def do(cls, expenses):
        pass


class HrExpenseLine(sequence_ordered(), ModelSQL, ModelView):
    "Expense Line"
    __name__ = 'hr.expense.line'

    company = fields.Function(
        fields.Many2One('company.company', "Company"),
        'on_change_with_company')
    expense = fields.Many2One(
        'hr.expense', "Expense", required=True,
        ondelete='CASCADE',
        states={
            'readonly': (
                (Eval('expense_state') != 'draft')
                & Bool(Eval('expense'))
            ),
        },
        depends=['expense_state'],
        help="Add the line below the expense.")
    expense_state = fields.Function(
        fields.Selection('get_expense_states', "Expense State"),
        'on_change_with_expense_state')
    product = fields.Many2One(
        'product.product', "Product", required=True,
        domain=[('is_hr_expense', '=', True)],
        states={'readonly': Eval('expense_state') != 'draft'},
        context={'company': Eval('company', None)},
        depends=['expense_state', 'company'])
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'on_change_with_currency')
    untaxed_amount = fields.Numeric(
        "Untaxed Cache", digits='currency', required=True,
        states={
            'readonly': Eval('expense_state') != 'draft',
        })
    tax_amount = fields.Numeric(
        "Tax Cache", digits='currency', required=True,
        states={
            'readonly': Eval('expense_state') != 'draft',
        })
    total_amount = fields.Numeric(
        "Total Cache", digits='currency', required=True,
        states={
            'readonly': Eval('expense_state') != 'draft',
        })
    purchase_date = fields.Date(
        "Purchase date", required=True,
        states={
            'readonly': Eval('expense_state') != 'draft'
        },
        depends=['expense_state'])
    party = fields.Many2One(
        'party.party', "Party", required=True,
        states={
            'readonly': Eval('expense_state') != 'draft',
        },
        context={
            'company': Eval('company', None),
        },
        depends=['expense_state', 'company'])
    reference = fields.Char(
        "Invoice reference", required=True,
        states={
            'readonly': Eval('expense_state') != 'draft',
        },
        depends=['expense_state'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.__access__.add('expense')

    @classmethod
    def get_expense_states(cls):
        pool = Pool()
        Expense = pool.get('hr.expense')
        return Expense.fields_get(['state'])['state']['selection']

    @classmethod
    def default_purchase_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('expense', '_parent_expense.company')
    def on_change_with_company(self, name=None):
        if self.expense and self.expense.company:
            return self.expense.company.id

    @fields.depends('expense', '_parent_expense.state')
    def on_change_with_expense_state(self, name=None):
        if self.expense:
            return self.expense.state

    @fields.depends('expense', '_parent_expense.currency')
    def on_change_with_currency(self, name=None):
        if self.expense and self.expense.currency:
            return self.expense.currency.id

    def get_tax_account(self):
        pool = Pool()
        config = pool.get('hr.expense.configuration')(1)
        return config.tax_account

    def create_account_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')
        label = (
            f"{self.expense.expense_date.month}/"
            f"{self.expense.expense_date.year} "
            f"{self.expense.employee.party.name} {self.reference}")
        period = Period.find(self.expense.company.id,
                             date=self.expense.expense_date)
        account = self.product.template.account_category.account_expense
        movelines = [
            Line(description=self.reference,
                 account=account,
                 origin=self,
                 debit=self.untaxed_amount),
            Line(description=self.reference,
                 account=self.get_tax_account(),
                 origin=self,
                 debit=self.tax_amount),
            Line(description=self.reference,
                 account=self.expense.get_supplier_account(),
                 origin=self,
                 credit=self.total_amount,
                 party=self.party),
        ]

        move = Move(
            journal=self.expense.get_purchase_journal(),
            date=self.expense.expense_date,
            description=label,
            lines=movelines,
            period=period,
            origin=self.expense,
        )
        move.save()
        return move


class ZipReport(Report):
    __name__ = 'hr.expense.zipreport'

    @classmethod
    def execute(cls, ids, data):
        pool = Pool()
        cls.check_access()

        Attachment = pool.get('ir.attachment')
        Line = pool.get('hr.expense.line')
        ids = list(map(int, ids))

        records = cls._get_records(ids, 'hr.expense', data)
        zip_id, zip_name = tempfile.mkstemp()
        export_dir = tempfile.TemporaryDirectory()
        for record in records:
            date_ = record.expense_date.strftime('%Y %m')
            party_ = record.employee.party.name
            dir_name = f'{date_} {party_} {record.id}'
            makedirs(path.join(export_dir.name, dir_name))
            attachments = Attachment.search([('resource', '=', record)])
            for attachment in attachments:
                data = attachment.data
                if data:
                    with open(
                        path.join(export_dir.name, dir_name, attachment.name),
                        'wb'
                    ) as fp:
                        fp.write(data)

            lines = Line.export_data(
                record.lines,
                [
                    'purchase_date',
                    'untaxed_amount',
                    'tax_amount',
                    'total_amount'
                ]
            )
            with open(
                path.join(export_dir.name, dir_name, 'export.csv'), 'w'
            ) as csvfile:
                spamwriter = csv.writer(csvfile)
                spamwriter.writerow(
                    ['Employee', 'Date', 'Untaxed amount', 'Tax', 'Amount'])
                for line in lines:
                    spamwriter.writerow([
                        party_,
                        line[0].isoformat(),
                        line[1],
                        line[2],
                        line[3],
                    ])

        zip_file = make_archive(export_dir.name, 'zip', export_dir.name)
        with open(zip_file, 'rb') as fp:
            content = fp.read()

        return (
            'zip',
            content,
            False,
            f'export expense {date.today().isoformat()}.zip',
        )
