from trytond.pool import PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        models_to_add = ['hr.expense']
        return super(Move, cls)._get_origin() + models_to_add


class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @classmethod
    def _get_origin(cls):
        models_to_add = ['hr.expense.line']
        return super(Line, cls)._get_origin() + models_to_add
